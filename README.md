# atom-humane-like-syntax theme

An Atom syntax-theme that is a clone and modification of Humane-Syntax by Damien Guard. Works in Atom and Pulsar.

![atom-humane-like-syntax.png](./atom-humane-like-syntax.png?raw=true "Screenshot")

## Installation

This theme is not available in the theme manager's download feature. However, the folder 'atom-humane-like-syntax' can be downloaded from the online repository and stored under the 'packages' folder for the Atom or Pulsar configuration. On Linux, for example, those locations would be ~/.atom/packages and ~/.pulsar/packages, respectively. Then restart the app; the theme should now be selectable in the theme manager.
