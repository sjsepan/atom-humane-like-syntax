# History

## 0.0.0 - First Release

* Initial port from Humane-Syntax

## 0.0.1

port from humane-syntax to atom-humane-like

## 0.0.2

rename to atom-humane-like-syntax and fix as recommended by Deprecation Cop in Atom
